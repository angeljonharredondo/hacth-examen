import { combineReducers } from 'redux'
import filterReducer from '../features/filters/filterSlice'
import homeworksReducer from "../features/homeworks/homeworksSlice";

export default combineReducers({
    homeworks: homeworksReducer,
    filter: filterReducer
})