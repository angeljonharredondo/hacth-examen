import React from 'react'
import PropTypes from 'prop-types'

// componente button element para el filtro
const Btn = ({ active, children, setFilter, filter }) => ( // recibe props registrados en FilterBtn
    <button type="button" onClick={() => setFilter(filter)} disabled={active} className="btn btn-secondary" >
        { children } 
    </button>
)
// validaciones de tipo de dato para recibir
Btn.propTypes = { 
    active: PropTypes.bool.isRequired,
    children: PropTypes.node.isRequired,
    setFilter: PropTypes.func.isRequired,
    filter: PropTypes.string.isRequired
}

export default Btn