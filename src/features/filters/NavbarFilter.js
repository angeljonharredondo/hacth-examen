import React from 'react'
import { ShowFilters } from './filterSlice'
import FilterBtn from './FilterBtn'
// componente que agrupa los botones de filtro
// se inforpora el prop filter usando las constantes registradas en el estado de filtros.
const NavbarFilter = () => (
    <div className="btn-group" role="group" >
        <FilterBtn filter={ShowFilters.ALL} >Todas</FilterBtn>
        <FilterBtn filter={ShowFilters.ACTIVE} >Activas</FilterBtn>
        <FilterBtn filter={ShowFilters.COMPLETED} >Completadas</FilterBtn>
    </div>
)

export default NavbarFilter