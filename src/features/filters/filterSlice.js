import { createSlice } from '@reduxjs/toolkit'
// filtros que se usarán 
export const ShowFilters = {
    ALL: 'ALL',
    COMPLETED : 'COMPLETED',
    ACTIVE: 'ACTIVE'
}
// Estado para manejo de filtros
const filterSlice = createSlice({
    name: 'filters',
    initialState: ShowFilters.ALL,
    reducers: {
        setFilter(state, { payload }) { return payload }
    } 
})
// action para la gestión de los filtros
export const { setFilter } = filterSlice.actions

export default filterSlice.reducer