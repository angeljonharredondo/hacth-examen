import { connect } from "react-redux"
import { setFilter } from "./filterSlice"
import Btn from './Btn'
// se evalua si el filtro elegido concuerda con el correspondiente al botòn para mandar TRUE o FALSE para el atributo disabled
const mapStateToProps = (state, ownProps) => ({
    active: ownProps.filter === state.filter
})
// se incorpora action para cambiar filtro cuando se de click al botón
const mapDispatchToProps = { setFilter }
// conectar props al componente Btn
export default connect(mapStateToProps, mapDispatchToProps)(Btn)