import { createSlice } from "@reduxjs/toolkit"
import moment from 'moment'
// estado de tareas
const homeworksSlice = createSlice({
    name: 'homeworks',
    initialState: [],
    reducers: {
        homeworkAdded(state,{payload}) { // action para agregr nueva tarea
            state.push(payload)
        },
        homeworkUpdated(state, {payload}) { // action para modificar una tarea
            const {id, title, status} = payload 
            const existingHomework = state.find(homework => homework.id === id) // obtener tarea seleccionada del arreglo
            if(existingHomework) { // validar que existe
                existingHomework.title = title 
                existingHomework.status = status
                existingHomework.updatedAt = moment(new Date()).format('DD/MM/YYYY hh:mm') // actualizar fecha de actualización
            }
        }
    }
})

export const { homeworkAdded, homeworkUpdated } = homeworksSlice.actions

export default homeworksSlice.reducer