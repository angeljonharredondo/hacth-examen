import React from 'react'
import PropTypes from 'prop-types'
import { ManageHomework } from "./ManageHomeworks";

const translateStatus = (status) => {
    let response = ''
    switch (status) {
        case 'active':
            response = 'Activo'
            break
        case 'canceled':
            response = 'Cancelada'
            break
        case 'completed':
            response = 'Completada'
            break
        default:
            response = 'Activo'
    }
    return response
}

const Homework = ({id, title, status, createdAt, updatedAt }) => (
    <tr>
        <td>{title}</td>
        <td>{translateStatus(status)}</td>
        <td>{createdAt}</td>
        <td>{updatedAt}</td>
        <td>
            <ManageHomework cssClass="btn btn-xs btn-info" btnTitle="Editar" value={{id, title, status, createdAt, updatedAt }} />
        </td>
    </tr>
)

Homework.propTypes = {
    id: PropTypes.string,
    title: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    updatedAt: PropTypes.string.isRequired
}

export default Homework