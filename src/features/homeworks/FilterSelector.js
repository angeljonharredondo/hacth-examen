import { connect } from "react-redux"
import { createSelector } from "@reduxjs/toolkit"
import HomeworksList from './HomeworksList'
import { ShowFilters } from '../filters/filterSlice'

const selectHomeworks = state => state.homeworks // se obtiene estado de tareas
const selectFilter = state => state.filter // se obtitne estado de filtros

const selectFilterHomeworks = createSelector( // crear selector para gestion de tareas con base en el filtro elegido
    [selectHomeworks, selectFilter],
    (homeworks, filter) => {
        var cleaned = homeworks.filter(h => h.status !== 'canceled') // quitar las tareas canceladas
        switch(filter) { // switch respecto al filtro seleccionado
            case ShowFilters.ALL:
                return cleaned
            case ShowFilters.COMPLETED:
                return cleaned.filter(h => h.status === 'completed') // se aplica filtro en referencia al status seleccionado
            case ShowFilters.ACTIVE:
                return cleaned.filter(h => h.status === 'active') // se aplica filtro en referencia al status seleccionado
            default:
                throw new Error('Unknown Filter: ' + filter)
        }
    }
)

const mapStateToProps = state => ({
    homeworks: selectFilterHomeworks(state) // se incorpora listado filtrado en el prop homeworks
})
// conectar selector con el componente que enlista las tareas
export default connect(mapStateToProps)(HomeworksList)

