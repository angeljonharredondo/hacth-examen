import React, { useState } from "react"
import { useDispatch } from 'react-redux'
import { nanoid } from '@reduxjs/toolkit'
import { homeworkAdded, homeworkUpdated } from './homeworksSlice'
import moment from "moment"

export const ManageHomework = ({value, cssClass, btnTitle}) => {
    const dispatch = useDispatch()
    // Extraer Elegida
    var homeworkSelected = value
    // validar si existe valor en la variable de la tarea seleccionada
    // Si no hay se inicializa objeto
    var homeworkBody = homeworkSelected || {
        id: nanoid(),
        title: '',
        status: 'active' // active, completed, canceled
    }
    var textAction = homeworkSelected ? 'Modificar' : 'Guardar'
    // generar getters y setters
    var [title, setTitle] = useState(homeworkBody.title)
    var [status, setStatus] = useState(homeworkBody.status)
    // aplicar setters onchange de inputs
    const onTitleChanged = e => setTitle(e.target.value)
    const onStatusChanged = e => setStatus(e.target.value)
    // abrir modal para agregar/actualizar tarea
    const openModal = () => {
        let modal = document.getElementById('modal-manage-homework-' + homeworkBody.id)
        let body = document.getElementsByTagName('body')[0]
        body.classList.toggle('modal-open')
        modal.classList.add('show')
        modal.style.visibility = 'visible'
        modal.style.paddingRight = '15px'
    }
    // cerrar modal 
    const closeModal = () => {
        let modal = document.getElementById('modal-manage-homework-' + homeworkBody.id)
        let body = document.getElementsByTagName('body')[0]
        body.classList.toggle('modal-open')
        modal.classList.remove('show')
        modal.style.visibility = 'hidden'
        modal.style.paddingRight = 0
    }
    // llamar al action correspondiente agregar/actualizar
    const onManageAction = () => {
        if (title && textAction === 'Guardar') { // valdaciones de existencia de title y a que acción debe de ser
            dispatch(
                homeworkAdded({
                    id: homeworkBody.id,
                    title,
                    status,
                    createdAt: moment(new Date()).format("DD/MM/YYYY hh:mm"),
                    updatedAt: moment(new Date()).format("DD/MM/YYYY hh:mm")
                })
            )

            setTitle('')
        } else if (title && textAction === 'Modificar') { // valdaciones de existencia de title y a que acción debe de ser
            dispatch(
                homeworkUpdated({
                    id: homeworkBody.id,
                    title,
                    status
                })
            )
        }
        closeModal();
    }

    return (
         <>
            <button className={cssClass} onClick={openModal} >{btnTitle}</button>
            <div id={`modal-manage-homework-${homeworkBody.id}`} className="modal fade" >
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title text-uppercase" id="exampleModalLabel">{`${textAction} Tarea`}</h5>
                            <button type="button" className="close" onClick={closeModal} aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body" >
                            <div className="row" >
                                <div className="col">
                                    <div className="form-group" >
                                        <input id={`title-hw-${homeworkBody.id}`} name={`title-hw-${homeworkBody.id}`} value={title} onChange={onTitleChanged} className="form-control" />
                                    </div>
                                    <div className="form-group" >
                                        <select id="status-hw" disabled={homeworkSelected ? '' : 'disabled'} name="status-hw" value={status} onChange={onStatusChanged} className="form-control" >
                                            <option value="active" >Activo</option>
                                            <option value="completed" >Completado</option>
                                            <option value="canceled" >Cancelado</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <button onClick={onManageAction} className="btn btn-success" >{textAction}</button> &nbsp;
                                        <button onClick={closeModal} className="btn" >Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}