import React from 'react'
import PropTypes from 'prop-types'
import Homework from './Homework'


const HomeworksList = ({homeworks}) => ( // Recibe prop que pasa por el selector donde se aplican filtros y limpia tareas canceladas
    <div className="table-responsive" >
        <table className="table table-striped" >
            <thead>
                <tr>
                    <th className="bg-info text-white">Tarea</th>
                    <th className="bg-info text-white">Estatus</th>
                    <th className="bg-info text-white">Fecha creación</th>
                    <th className="bg-info text-white">última modificación</th>
                    <th className="bg-info text-white">Acciones</th>
                </tr>
            </thead>
            <tbody>
                {
                    homeworks.map(homework => (
                        <Homework key={homework.id} {...homework} /> // incorpora datos de la tarea como prop en el componente
                    ))
                }
            </tbody>
        </table>
    </div>
)

HomeworksList.propTypes = { // validaciones de tipo de datos que recibira por parte del arreglo de tareas
    homeworks: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string.isRequired,
            title: PropTypes.string.isRequired,
            status: PropTypes.string.isRequired,
            createdAt: PropTypes.string.isRequired,
            updatedAt: PropTypes.string.isRequired
        }).isRequired
    ).isRequired
}

export default HomeworksList