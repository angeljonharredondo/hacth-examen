import React from 'react';
import './App.css';
import { ManageHomework } from './features/homeworks/ManageHomeworks'
import FilterSelector from './features/homeworks/FilterSelector'
import NavbarFilter from './features/filters/NavbarFilter'


function App() {
  return (
    <div className="App">
      <header className="container">
        <div className="row" >
          <div className="col-12">
            <h1 className="mt-5 text-uppercase text-center text-info font-weight-bolder"> Tareas por hacer</h1>
          </div>
          <div className="col-12 text-center" >
            <NavbarFilter />
          </div>
          <div className="col-12">
            <FilterSelector />
          </div>
          <ManageHomework cssClass="btn btn-info btn-lg btn-fixed" btnTitle="+ Nueva Tarea" value={null} />
        </div>
      </header>
    </div>
  );
}

export default App;
