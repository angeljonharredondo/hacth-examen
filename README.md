## HATCH EXAMEN

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template.

## Available Scripts

In the project directory, you can run:

### `yarn start`
### `yarn test`
### `yarn build`
### `yarn eject`

## HORAS EFECTUADAS POR FUNCIÓN

Después de llevar unas 5 horas aproximadamente viendo tutoriales y leyendo la documentación de la página oficial de React Js y Redux.
Para hacer este examen me ha llevado lo siguiente: 

### Generar cortes de estados (states,actions) tareas y filtros - 1 hora y 35 minutos
### Generar componentes referentes a las tareas - 40 minutos
### Generar componentes referentes a los filtros - 1 hora
### Juntar todos los componentes y hacer que funcione todo - 1 hora

